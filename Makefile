# comment this out if you don't have libpng installed
PNG	= yes



CC	= gcc
CFLAGS	= -O2 -g -Wall `pkg-config gtk+-2.0 --cflags`

LD	= $(CC)
LDFLAGS	= `pkg-config gtk+-2.0 --libs` -lm

ifeq ($(PNG),yes)
CFLAGS	+= -DHAVE_LIBPNG
LDFLAGS	+= -lpng
endif

SRCS	= xnecview.c xwin.c parse_input.c parse_output.c draw.c draw_opaque.c freqplot.c postscript.c icon.xbm
OBJS	= xnecview.o xwin.o parse_input.o parse_output.o draw.o draw_opaque.o freqplot.o postscript.o

all:	xnecview

xnecview:	$(OBJS)
	$(CC) $(CFLAGS) $(LDFLAGS) $(OBJS)  -o xnecview

xnecview.tgz:	$(SRCS)
	tar czvf xnecview.tgz COPYING README HISTORY Imakefile Imakefile_nopng Makefile $(SRCS) xnecview.h config.h xnecview.1x xnecview.man

